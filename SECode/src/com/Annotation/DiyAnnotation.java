package com.Annotation;

import java.lang.annotation.*;

/**
 * 自定义注解
 */
public class DiyAnnotation {
    //注解可以显式赋值，没有默认值就必须赋值
    @DiyAnnotataion(name = "hh",age = 6,hh={"年号","你好"})
    public void test(){

    }
}
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@interface DiyAnnotataion{
    //注解的参数 ：参数类型+参数名（）
    String name() default "";
    int age() default 2;
    int id() default -1;//如果默认值为-1，代表不存在
    String[] hh();

}
