package com.Annotation;

import javax.annotation.Resource;
import java.lang.annotation.*;

//测试元注解
@MyAnnotataion
public class Meta_Annotation {
    @MyAnnotataion
    public void test(){

    }
}
//定义一个注解
@Target({ElementType.METHOD,ElementType.TYPE})//可以在方法上或者类上使用
@Retention(RetentionPolicy.RUNTIME)//运行时生效
@Documented  //表示注解是否会在JavaDoc中生成
@Inherited //子类可以继承父类的注解
@interface MyAnnotataion{

}