package com.JUC.readWriteLock;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @Author Feng
 * @Date 2021/10/21 16:24
 * @Version 1.0
 * @Description 读写锁的测试类
 */
public class ReadWriteLockDemo {
    public static void main(String[] args) {
        //MyCache myCache = new MyCache();
        MyCacheLock  myCache = new MyCacheLock();

        //写入操作
        for (int i = 1; i <= 5; i++) {
            final int temp=i;
            new Thread(()->{
                myCache.put(temp+"",temp+"");
            },String.valueOf(i)).start();
        }
        //读取操作
        for (int i = 1; i <= 5; i++) {
            final int temp=i;
            new Thread(()->{
                myCache.get(temp+"");
            },String.valueOf(i)).start();
        }
    }
}

/**
 * 自定义缓存类
 */
class MyCache {
    // volatile :1.保证可见性 2.防止指令重排
    private volatile Map<String, Object> map = new HashMap<>();

    //存 ，写
    public void put(String key, Object value) {
        System.out.println(Thread.currentThread().getName() + "开始写入"+value);
        map.put(key, value);
        System.out.println(Thread.currentThread().getName() + "写入完毕！");
    }

    //取 ，读
    public void get(String key) {
        System.out.println(Thread.currentThread().getName() + "开始读取！");
        Object o = map.get(key);
        System.out.println(Thread.currentThread().getName() + "读取完毕！");
    }
}

/**
 * 加锁自定义缓存类
 * 独占锁（写锁):一次只能被一个线程占有
 * 共亨锁（读锁）:多个线程可以同时占有
 */
class MyCacheLock {
    // volatile :1.保证可见性 2.防止指令重排
    private volatile Map<String, Object> map = new HashMap<>();

    //读写锁，更加细粒度的去控制，也就是可以控制是读锁，还是写锁
    private ReadWriteLock readWriteLock=new ReentrantReadWriteLock();

    //存 ，写  ,只希望同时只有一个线程写，写写互斥，不能共存
    public void put(String key, Object value) {
        readWriteLock.writeLock().lock();//写锁上锁
        try {
            //业务代码
            System.out.println(Thread.currentThread().getName() + "开始写入"+value);
            map.put(key, value);
            System.out.println(Thread.currentThread().getName() + "写入完毕！");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            readWriteLock.writeLock().unlock();//写锁解锁
        }
    }

    //取 ，读，所有人都可以读，读读共享 ，可以共存
    public void get(String key) {
        //为啥所有人都可以读，为啥要加锁，因为防止读的时候又去写，读写互斥，不能共存
        readWriteLock.readLock().lock();
        try {
            System.out.println(Thread.currentThread().getName() + "开始读取！");
            Object o = map.get(key);
            System.out.println(Thread.currentThread().getName() + "读取完毕！");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            readWriteLock.readLock().unlock();
        }
    }
}
