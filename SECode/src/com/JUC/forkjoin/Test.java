package com.JUC.forkjoin;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.stream.LongStream;

/**
 * @Author Feng
 * @Date 2021/10/25 21:42
 * @Version 1.0
 * @Description 测试类
 */
public class Test {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //test1();// 7253ms
        //test2(); //10028
        test3();//155
    }

    //普通的方法
    public static void test1() {
        Long sum = 0L;
        Long start = System.currentTimeMillis();
        for (Long i = 0L; i <= 10_0000_0000; i++) {
            sum += i;
        }
        Long end = System.currentTimeMillis();
        System.out.println("sum="+sum+"花费时间为:" + (end - start));
    }

    // 使用 ForkJoin
    public static void test2() throws ExecutionException, InterruptedException {
        Long start = System.currentTimeMillis();
        ForkJoinPool forkJoinPool = new ForkJoinPool();
        ForkJoinDemo task = new ForkJoinDemo(0L, 10_0000_0000L);
        // forkJoinPool.execute(task);执行任务，没有返回值
        ForkJoinTask<Long> submit = forkJoinPool.submit(task);//提交任务
        Long sum = submit.get();
        Long end = System.currentTimeMillis();
        System.out.println("sum="+sum+"花费时间为:" + (end - start));
    }

    //Stream 并行流
    public static void test3() {
        Long start = System.currentTimeMillis();
        // rangeClosed 两个参数就是 开区间和闭区间 ，parallel:并行执行 ，reduce:规约计算获取结果 Long::sum表示Long下的求和方法
        long sum = LongStream.rangeClosed(0L, 10_0000_0000L).parallel().reduce(0, Long::sum);
        Long end = System.currentTimeMillis();
        System.out.println("sum="+sum+"花费时间为:" + (end - start));
    }
}
