package com.JUC.forkjoin;

import java.util.concurrent.RecursiveTask;

/**
 * @Author Feng
 * @Date 2021/10/25 20:56
 * @Version 1.0
 * @Description ForkJoin测试类
 * <p>
 * 如何使用 ForkJoin
 * 1. ForkJoinPool
 * 2. 新建一个任务: ForkJoinPool.execute(ForkJoinTask<?> task)
 * 3. 计算类要继承 RecursiveTask<>
 * 求和计算的任务
 */
public class ForkJoinDemo extends RecursiveTask<Long> {
    private Long start; //1
    private Long end; // 10_0000_0000
    //临界值
    private final Long temp = 10000L;

    public ForkJoinDemo(Long start, Long end) {
        this.start = start;
        this.end = end;
    }

    //这个方法就是通过临界值判断，数值小的时候直接算，数值大的时候就给ForkJoin
    //计算的方法
    @Override
    protected Long compute() {
        if ((end - start) < temp) {
            Long sum = 0L;
            for (Long i = start; i <= end; i++) {
                sum += i;
            }
            return sum;
        }
        //数据大的时候，进入分支计算 forkJoin
        Long middle = (end + start) / 2;//中间值
        //拆分成两个任务
        ForkJoinDemo task1 = new ForkJoinDemo(start, middle);
        //拆分任务，把任务压入线程队列
        task1.fork();
        ForkJoinDemo task2 = new ForkJoinDemo(middle+1, end);
        task2.fork();
        //合并
        long l = task1.join() + task2.join();
        return l;
    }
}
