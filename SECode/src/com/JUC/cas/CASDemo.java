package com.JUC.cas;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicStampedReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author Feng
 * @Date 2021/10/28 16:10
 * @Version 1.0
 * @Description CAS测试类
 */
public class CASDemo {


    // CAS : compareAndSet 比较并交换
    //  compareAndSet()源码
    //  public final boolean compareAndSet(int expect, int update) {
    //        return unsafe.compareAndSwapInt(this, valueOffset, expect, update);
    //    }
    //    如果当前值==预期值，则原子地将值设置为给定的更新值。
    //  参数：
    //      期望 - 期望值
    //      更新 - 新值
    //  返回：
    //      如果成功则为true 。 假返回表示实际值不等于预期值

//    public static void main(String[] args) {
//        AtomicInteger atomicInteger = new AtomicInteger(2020);
//        System.out.println(atomicInteger.get());//2020
//        // 如果我的期望值与创建实例的时候的值一样就更新，否则就不更新,CAS是CPU的并发原语
//        System.out.println(atomicInteger.compareAndSet(2020, 2021));// true
//        System.out.println(atomicInteger.get());// 2021
//
//        atomicInteger.getAndIncrement();// ++操作  以原子方式将当前值递增 1
//
//
//        System.out.println(atomicInteger.compareAndSet(2020, 2021));// false
//        System.out.println(atomicInteger.get());// 2021
//    }


    public static void main(String[] args) {
        // AtomicStampedReference 时间戳引用
        // 两个参数 初始值和时间戳
        // 注意泛型包装类对象的引用问题，这里值超过 -128~127 就会出现包装类的地址比较问题 ==
        AtomicStampedReference<Integer> atomicReference = new AtomicStampedReference<>(100,1);


        new Thread(()->{
            System.out.println("初始A版本===>"+atomicReference.getStamp());//初始版本

            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // compareAndSet更新初始值和版本号
            System.out.println(atomicReference.compareAndSet(100, 102
                    , atomicReference.getStamp()
                    , atomicReference.getStamp() + 1)+"A");
            System.out.println("A1===>"+atomicReference.getStamp());

            // 第二次更新回来
            System.out.println(atomicReference.compareAndSet(102, 100
                    , atomicReference.getStamp()
                    , atomicReference.getStamp() + 1)+"A");
            System.out.println("A2===>"+atomicReference.getStamp());

        },"A").start();
        new Thread(()->{

            System.out.println("B初始版本==>"+atomicReference.getStamp());
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(atomicReference.compareAndSet(100, 103
                    , atomicReference.getStamp()
                    , atomicReference.getStamp() + 1)+"B");
            System.out.println("B更新===>"+atomicReference.getStamp());
        },"B").start();


    }
}
