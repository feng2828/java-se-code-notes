package com.JUC.callable;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @Author Feng
 * @Date 2021/10/21 9:30
 * @Version 1.0
 * @Description
 */
public class CallableTest {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //那么怎么启动Callable呢?
        //  new Thread(new Runnable()).start();
        //  new Thread(new FutureTask<v>()).start();
        //  new Thread(new FutureTask<v>(Callable<V> callable)).start();

        MyThread myThread = new MyThread();
        //适配类  futureTask
        FutureTask futureTask = new FutureTask(myThread);
        new Thread(futureTask,"A").start();
        new Thread(futureTask,"B").start();

        //获取Callable的返回结果
        String o = (String)futureTask.get();//这个 get方法可能会造成阻塞
        System.out.println(o);


    }
}
class MyThread implements Callable<String> {

    @Override
    public String call() throws Exception {
        return "123";
    }
}