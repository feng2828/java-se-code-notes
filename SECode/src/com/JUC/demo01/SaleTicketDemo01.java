package com.JUC.demo01;

/**
 * @Author Feng
 * @Date 2021/10/9 9:48
 * @Version 1.0
 * @Description 基本的卖票例子
 */
//真正的多线程开发，公司中的开发，注重降低耦合性
//线程就是一个单独的资源类,就是分离出来单独作为一个资源使用，这个类没有任何其他附属的操作!
// 1、属性、方法
public class SaleTicketDemo01 {
    public static void main(String[] args) {
        //并发编程：多个线程操作同一个资源
        Ticket ticket = new Ticket();
        //因为 Thread的参数是一个是Runnable的实现类
        //Runnable是一个函数式接口@FunctionalInterface，所以可以用 Lambda表达式
        new Thread(()->{
            for (int i = 0; i < 60; i++) {
                ticket.sale();
            }
        },"A").start();
        new Thread(()->{
            for (int i = 0; i < 60; i++) {
                ticket.sale();
            }
        },"B").start();
        new Thread(()->{
            for (int i = 0; i < 60; i++) {
                ticket.sale();
            }
        },"C").start();
    }
}
//资源类 OOP
class Ticket{
    //属性
    private int number=50;
    //卖票方法
    //synchronize本质就是: 队列 锁
    public synchronized void sale(){
        if(number>0){
            System.out.println(Thread.currentThread().getName()+"卖了第"+(number--)+"张票，剩余"+number+"张票");
        }
        synchronized (Ticket.class){

        }
    }
}
