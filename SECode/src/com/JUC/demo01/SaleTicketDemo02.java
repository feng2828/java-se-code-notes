package com.JUC.demo01;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author Feng
 * @Date 2021/10/9 9:48
 * @Version 1.0
 * @Description 基本的卖票例子
 */
//真正的多线程开发，公司中的开发，注重降低耦合性
//线程就是一个单独的资源类,就是分离出来单独作为一个资源使用，这个类没有任何其他附属的操作!
// 1、属性、方法
public class SaleTicketDemo02 {
    public static void main(String[] args) {
        Ticket02 ticket02 = new Ticket02();
        new Thread(() -> {
            for (int i = 0; i < 60; i++) {
                ticket02.sale();
            }
        }, "A").start();
        new Thread(() -> {
            for (int i = 0; i < 60; i++) {
                ticket02.sale();
            }
        }, "B").start();
        new Thread(() -> {
            for (int i = 0; i < 60; i++) {
                ticket02.sale();
            }
        }, "C").start();
    }
}

//LOCK
//LOCk锁的三部曲：
// 1.new ReentrantLock()
// 2.lock.lock();//加锁
// 3.finally ==> lock.unlock();//解锁
class Ticket02 {
    //属性
    private int number = 50;

    Lock lock = new ReentrantLock();

    //卖票方法
    public void sale() {
        lock.lock();//加锁
        try {
            //业务代码
            if (number > 0) {
                System.out.println(Thread.currentThread().getName() + "卖了第" + (number--) + "张票，剩余" + number + "张票");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();//解锁
        }
    }

}
