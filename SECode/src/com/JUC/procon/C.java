package com.JUC.procon;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author Feng
 * @Date 2021/10/20 9:43
 * @Version 1.0
 * @Description :Condition的使用
 */
public class C {
    public static void main(String[] args) {
        Data3 data3 = new Data3();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                data3.printA();
            }
        }, "A").start();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                data3.printB();
            }
        }, "B").start();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                data3.printC();
            }
        }, "C").start();
    }
}

class Data3 {
    private int number = 1;//标志位
    private Lock lock = new ReentrantLock();
    Condition condition1 = lock.newCondition();//用三个Condition分别监视
    Condition condition2 = lock.newCondition();
    Condition condition3 = lock.newCondition();
    public void printA() {
        lock.lock();
        try {
            //业务代码 ：1.判断条件 2.执行代码 3.唤醒通知
            while (number!=1){
                condition1.await();
            }
            number=2;//改变标志位为2
            System.out.println(Thread.currentThread().getName()+"==>AAAAAAA");
            condition2.signal();//执行完唤醒B线程
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
    public void printB() {
        lock.lock();
        try {
            //业务代码 ：1.判断条件 2.执行代码 3.唤醒通知
            while (number!=2){
                condition2.await();//标志为不是2就等待
            }
            number=3;//改变标志位为2
            System.out.println(Thread.currentThread().getName()+"==>BBBBBBB");
            condition3.signal();//执行完唤醒C线程
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
    public void printC() {
        lock.lock();
        try {
            //业务代码 ：1.判断条件 2.执行代码 3.唤醒通知
            while (number!=3){
                condition3.await();
            }
            number=1;//改变标志位为1
            System.out.println(Thread.currentThread().getName()+"==>CCCCCCC");
            condition1.signal();//执行完唤醒A线程
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}

//也可以用一个Condition来实现，但是这种是全部唤醒等待的线程，然后通过条件来限制是否继续等待或者执行
// 跟上面的这种唤醒单个的signal（），本质上是不一样的，用单个的是为了更加精准的针对某一个线程实现唤醒
//class Data3 {
//    private int number = 1;//标志位
//    private Lock lock = new ReentrantLock();
//    Condition condition = lock.newCondition();
//
//    public void printA() {
//        lock.lock();
//        try {
//            //业务代码 ：1.判断条件 2.执行代码 3.唤醒通知
//            while (number!=1){
//                condition.await();
//            }
//            number=2;//改变标志位为2
//            System.out.println(Thread.currentThread().getName()+"==>AAAAAAA");
//            condition.signalAll();//执行完唤醒全部线程
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            lock.unlock();
//        }
//    }
//    public void printB() {
//        lock.lock();
//        try {
//            //业务代码 ：1.判断条件 2.执行代码 3.唤醒通知
//            while (number!=2){
//                condition.await();//标志为不是2就等待
//            }
//            number=3;//改变标志位为2
//            System.out.println(Thread.currentThread().getName()+"==>BBBBBBB");
//            condition.signalAll();//执行完唤醒全部线程
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            lock.unlock();
//        }
//    }
//    public void printC() {
//        lock.lock();
//        try {
//            //业务代码 ：1.判断条件 2.执行代码 3.唤醒通知
//            while (number!=3){
//                condition.await();
//            }
//            number=1;//改变标志位为1
//            System.out.println(Thread.currentThread().getName()+"==>CCCCCCC");
//            condition.signalAll();//执行完唤醒全部线程
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            lock.unlock();
//        }
//    }
//}
