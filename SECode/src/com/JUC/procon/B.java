package com.JUC.procon;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author Feng
 * @Date 2021/10/20 9:10
 * @Version 1.0
 * @Description JUC版的生产者消费者
 */
public class B {
    public static void main(String[] args) {
        Data2 data = new Data2();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    data.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "A").start();
        //B线程减一
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    data.decrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "B").start();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    data.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "C").start();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    data.decrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "D").start();

    }

}


//JUC生产者消费者的过程
// 1.上锁 2.业务代码 3.等待和唤醒 4.解锁
class Data2 {//数字，资源类

    private int num = 0;
    Lock lock = new ReentrantLock();
    Condition condition = lock.newCondition();


    // +1的方法
    public void increment() throws InterruptedException {
        lock.lock();
        try {
            while (num != 0) {
                condition.await();//不等于0的时候等待
            }
            //等于0的时候加1
            num++;
            System.out.println(Thread.currentThread().getName() + "==>" + num);
            condition.signalAll();//加完后唤醒
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    // -1的方法
    public void decrement() throws InterruptedException {
        lock.lock();
        try {
            while (num == 0) {
                condition.await();//不等于0的时候等待
            }
            //等于1的时候减1
            num--;
            System.out.println(Thread.currentThread().getName() + "==>" + num);
            condition.signalAll();//减完后唤醒其他线程
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

}
