package com.JUC.procon;

/**
 * @Author Feng
 * @Date 2021/10/19 10:04
 * @Version 1.0
 * @Description :传统的synchronized版本
 * 线程之间的通信问题:生产者和消费者问题 !   等待唤醒，通知唤醒
 * 线程交替执行:A线程和B线程，操作同一个变量 num = 0
 * A:num+1
 * B:num-1
 */
public class A {
    public static void main(String[] args) {
        Data data = new Data();
        //A线程加1
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    data.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "A").start();
        //B线程减一
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    data.decrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "B").start();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    data.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "C").start();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    data.decrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "D").start();
    }
}


//生产者消费者的三部曲
// 1.判断等待 2.业务代码 3.通知唤醒
class Data {//数字，资源类

    private int num = 0;

    // +1的方法
    public synchronized void increment() throws InterruptedException {
        while (num != 0) {
            this.wait();//不等于0的时候等待
        }
        //等于0的时候加1
        num++;
        System.out.println(Thread.currentThread().getName() + "==>" + num);
        this.notifyAll();//加完后唤醒
    }

    // -1的方法
    public synchronized void decrement() throws InterruptedException {
        while (num == 0) {
            this.wait();//不等于0的时候等待
        }
        //等于1的时候减1
        num--;
        System.out.println(Thread.currentThread().getName() + "==>" + num);
        this.notifyAll();//减完后唤醒其他线程
    }

}
