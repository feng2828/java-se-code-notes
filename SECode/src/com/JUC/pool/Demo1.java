package com.JUC.pool;

import java.util.concurrent.*;

/**
 * @Author Feng
 * @Date 2021/10/25 9:24
 * @Version 1.0
 * @Description Executors测试类
 */

/**
 * Executors工具类 ，三大方法
 */
public class Demo1 {
    public static void main(String[] args) {
        //ExecutorService threadPool = Executors.newSingleThreadExecutor();// 单个线程
        //ExecutorService threadPool=Executors.newFixedThreadPool(5);//创建一个固定大小的线程池
        //ExecutorService threadPool=Executors.newCachedThreadPool();//可变大小的线程池，遇强则强，遇弱则弱
        //自定义线程池

        ExecutorService threadPool=new ThreadPoolExecutor(
                2,
                Runtime.getRuntime().availableProcessors(),
                3,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<>(3),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy()//银行满了，还有人进来，不处理这个人的，抛出异常
                 );

        try {
            for (int i = 0; i < 9; i++) {
                threadPool.execute(() -> {
                    System.out.println(Thread.currentThread().getName() + "===> OK!");
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            threadPool.shutdown();
        }
    }
}
