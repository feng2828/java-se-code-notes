package com.JUC.unSafe;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author Feng
 * @Date 2021/10/21 8:54
 * @Version 1.0
 * @Description map的不安全测试类
 */
public class MapTest {
    public static void main(String[] args) {

        //map是这样用的嘛？不是，工作中不用Hashmap,在多线程里是不安全的，但是一般用来传Json
        //Map<String,String> map=new HashMap<>();
        //hashMap默认等价于什么？new HashMap<>(16,0.75);
        //这是初始化容量和加载因子  ，因为map扩容性能消耗大，指定容量避免扩容提高效率

        Map<String,String> map=new ConcurrentHashMap<>();
        for (int i = 1; i <= 20; i++) {
            new Thread(()->{
                map.put(Thread.currentThread().getName(), UUID.randomUUID().toString().substring(0,5));
                System.out.println(map); //  java.util.ConcurrentModificationException
            },String.valueOf(i)).start();
        }

    }
}
