package com.JUC.unSafe;


import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @Author Feng
 * @Date 2021/10/20 15:24
 * @Version 1.0
 * @Description List不安全的测试类
 */

//java.util.ConcurrentModificationException :并发修改异常
public class ListTest {
    public static void main(String[] args) {
        //并发下，ArrayList是不安全的
        //解决方案：
        // 1.用 Vector 创建List  List<String> list = new Vector<>();
        // 2.List<String> list = Collections.synchronizedList(new ArrayList<>());
        // 3.List<String> list = new CopyOnWriteArrayList<>();
        //List<String> list = new ArrayList<>();

        // CopyOnWrite ：写入时复制 COW 是计算机程序设计领域的一种优化策略
        //多个线程调用的时候，List是唯一的,读取的时候是 固定的 ，但是写入的时候 ，会怕出现覆盖的问题
        //在写入的时候避免覆盖，造成数据不一致的问题
        // COW就是在写入的时候先复制一份给调用者，防止原来的数据被覆盖，再进行写入创建一个新的List
        List<String> list = new CopyOnWriteArrayList<>();
        for (int i = 1; i <= 10; i++) {
            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,5));
                System.out.println(list);
            },String.valueOf(i)).start();
        }
    }
}
