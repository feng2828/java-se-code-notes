package com.JUC.lock8;

import java.util.concurrent.TimeUnit;

/**
 * @Author Feng
 * @Date 2021/10/20 10:34
 * @Version 1.0
 * @Description 8锁：锁的8个问题
 * <p>
 * 1.标准情况下，两个线程是 发短信先执行，再打电话
 * 2.假设在发短信的方法里延迟4秒，先打印的是谁？ 依旧是先发短信
 * 为什么会这样呢？  因为：有锁的存在
 * <p>
 * 3.新增了一个普通方法 :那么先执行的是哪个呢
 * 结果是：先Hello出来，再发短信，原因可能因为这个发短信休眠了4秒，将休眠去了，就先发短信了
 */
public class Test2 {
    public static void main(String[] args) {
        Phone2 phone2 = new Phone2();
        new Thread(() -> {
            phone2.SendSms();
        }, "A").start();

        //休眠1秒
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(() -> {
            phone2.hello();
        }, "B").start();
    }
}

class Phone2 {//资源类，电话

    //synchronized :锁的对象是方法的调用者 ，直白说就是谁调用就锁谁
    // 意思就是。锁的是这个Phone的对象，一个对象只有一把锁，也就是两个方法用的是同一把锁，线程谁先拿到就水下谁先执行
    //两个线程操作一个对象的两个，这两个同步方法都是锁的这个对象的this锁
    public synchronized void SendSms() {
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("发短信！");
    }

    public synchronized void Call() {
        System.out.println("打电话!");
    }

    public void hello() {
        System.out.println("Hello!");
    }

}
