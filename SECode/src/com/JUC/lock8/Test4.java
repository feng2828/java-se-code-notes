package com.JUC.lock8;

import java.util.concurrent.TimeUnit;

/**
 * @Author Feng
 * @Date 2021/10/20 10:34
 * @Version 1.0
 * @Description 8锁：锁的8个问题
 * <p>
 * 1.标准情况下，两个线程是 发短信先执行，再打电话
 * 2.假设在发短信的方法里延迟4秒，先打印的是谁？ 依旧是先发短信
 * 为什么会这样呢？  因为：有锁的存在
 * <p>
 * 3.新增了一个普通方法 :那么先执行的是哪个呢
 * 结果是：先Hello出来，再发短信，原因可能因为这个发短信休眠了4秒，将休眠去了，就先发短信了，由时间片决定
 * 4.假设有两个不同的对象时，结果会怎么样呢？
 * 结果 ：先打电话，再发短信 ，原因好像还是休眠时间，因为这是两个不同的调用者，是两把不同的锁，主要由时间片决定
 * 上面总结：假如是同一个对象调用，或者说是同一把锁，那么就是哪个同步方法先被调用就先被上锁执行，
 * 如果是普通方法与同步方法，或者是不同的对象调用，就是用的不是同一个锁，或者没有锁，就是按时间片执行，谁耗时少，谁先执行
 * 5.增加两个静态的同步方法
 * 静态方法：类一加载就有了，就是反射的那一个类的模板的方法，不管有几个对象，静态方法只有一个
 * 静态方法的锁：锁的是唯一的Class对象 ，也就是锁类的模板
 * 由于两个方法所得都是这个Class,也就是用的同一个锁，所以顺序调用
 * 6.假设是两个对象，并且还是两个静态的同步方法
 * 结果：依然是先发短信，因为不管怎么使用不同的对象，他们用的都是同一把锁，因为锁的那个Class是全局唯一的
 */
public class Test4 {
    public static void main(String[] args) {
        Phone4 phone = new Phone4();
        Phone4 phone2 = new Phone4();

        new Thread(() -> {
            phone.SendSms();
        }, "A").start();

        //休眠1秒
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(() -> {
            phone2.Call();
        }, "B").start();
    }
}

class Phone4 {//资源类，电话

    //静态的同步锁：锁的是全局唯一的Class模板
    public static synchronized void SendSms() {
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("发短信！");
    }

    public static synchronized void Call() {
        System.out.println("打电话!");
    }


}
