package com.JUC.future;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * @Author Feng
 * @Date 2021/10/26 8:53
 * @Version 1.0
 * @Description Future 测试类
 *   // 异步调用 ：CompletableFuture 类似于Ajax
 *   // 异步执行
 *   // 成功回调
 *   // 失败回调
 *
 *
 */
public class Demo01 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        /*一、这是没有返回值的 runAsync 异步回调*/

        // 发起一个异步请求
        // Void是void的包装类
        // CompletableFuture.runAsync(Runnable runnable)返回一个新的 CompletableFuture
//        CompletableFuture<Void> completableFuture = CompletableFuture.runAsync(()->{
//            // 睡眠模拟耗时的操作
//            try {
//                TimeUnit.SECONDS.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            System.out.println(Thread.currentThread().getName()+"runAsync==>Void");
//        });
//
//        TimeUnit.SECONDS.sleep(4);
//
//        // 按照道理应该是等待主线程的任务先输出，但是这里是异步的，他会先执行异步任务，他快一点的时候就先输出，形成异步
//         // 结果 ：先输出 ForkJoinPool.commonPool-worker-9runAsync==>Void
//        // 再输出 主线程任务！
//        System.out.println("主线程任务！");
//        completableFuture.get();//获取执行结果

        /*二、这是有返回值的  supplyAsync 异步回调*/

        // 成功和失败
        // 失败会返回错误的信息
        CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(()->{
            System.out.println(Thread.currentThread().getName()+"supplyAsync==>Integer");
            int i=1/0;
            return 1024;
        });
        // completableFuture的一个方法是 当`编译成功后` whenComplete，参数是 BiConsumer 就是两个参数的 Consumer
        // exceptionally 是`编译失败` ，参数是 Function<Throwable, ? extends T> fn), Function传入的参数是异常
        // 也就是 exceptionally的那个e 是异常
        // get 得到结果
        System.out.println(completableFuture.whenComplete((t, u) -> {
            System.out.println("t==>" + t); //正常的返回结果  t 是1024 ，u 是null
            System.out.println("u==>" + u); // 错误的返回胡结果 ，t是null , u是异常信息
        }).exceptionally((e) -> {
            System.out.println(e.getMessage());//打印异常信息
            return 233; // 错误的时候可以获取到错误的结果
        }).get());
    }
}
