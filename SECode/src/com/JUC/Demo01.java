package com.JUC;

/**
 * @Author Feng
 * @Date 2021/10/8 10:24
 * @Version 1.0
 * @Description
 */
public class Demo01 {
    public static void main(String[] args) {
        //获取CPU的核数
        //CPU 密集型 ，IO密集型
        System.out.println(Runtime.getRuntime().availableProcessors());
        //结果： 12


    }
}
