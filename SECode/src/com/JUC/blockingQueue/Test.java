package com.JUC.blockingQueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @Author Feng
 * @Date 2021/10/22 9:38
 * @Version 1.0
 * @Description 阻塞队列  的四种API
 */
public class Test {
    public static void main(String[] args) throws InterruptedException {
        test4();
    }


    /**
     * 1.抛出异常的
     */
    public static void test1() {
        //参数：容量 - 此队列的容量
        ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);

        //添加操作
        System.out.println(blockingQueue.add("A"));//true
        System.out.println(blockingQueue.add("B"));//true
        System.out.println(blockingQueue.add("C"));//true

        //抛出异常 : java.lang.IllegalStateException: Queue full 队列已满
        //System.out.println(blockingQueue.add("D"));

        System.out.println("==========");

        //查看队首元素
        System.out.println(blockingQueue.element());//A

        //移除，是FIFO，所以还是顺序出来  A,B,C
        System.out.println(blockingQueue.remove());//A
        System.out.println(blockingQueue.remove());//B
        System.out.println(blockingQueue.remove());//C

        //队空再取就会报异常：java.util.NoSuchElementException
        //System.out.println(blockingQueue.remove());


    }

    /**
     * 2.不抛出异常的,有返回值
     */
    public static void test2() {
        ArrayBlockingQueue<Object> blockingQueue = new ArrayBlockingQueue<>(3);

        System.out.println(blockingQueue.offer("A"));
        System.out.println(blockingQueue.offer("B"));
        System.out.println(blockingQueue.offer("C"));
        //队满时，不会抛出异常，返回false
        System.out.println(blockingQueue.offer("D"));

        //查看队首元素
        System.out.println(blockingQueue.peek());//A

        System.out.println("==========================");

        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
        //队空时，不会抛出异常，会返回null
        System.out.println(blockingQueue.poll());

    }

    /**
     * 3.等待，阻塞 （一直阻塞）
     */
    public static void test3() throws InterruptedException {

        ArrayBlockingQueue<Object> blockingQueue = new ArrayBlockingQueue<>(3);

        //存,这个没有返回值
        blockingQueue.put("A");
        blockingQueue.put("B");
        blockingQueue.put("C");
        //blockingQueue.put("D");//队列满了就会一直阻塞，然后程序一直在运行等待
        System.out.println("===============");
        //取
        System.out.println(blockingQueue.take());
        System.out.println(blockingQueue.take());
        System.out.println(blockingQueue.take());
        System.out.println(blockingQueue.take());//同样的，队空就一直阻塞着
    }

    /**
     * 4.等待，阻塞 （等待超时，就不等了）
     */
    public static void test4() throws InterruptedException {
        ArrayBlockingQueue<Object> blockingQueue = new ArrayBlockingQueue<>(3);
        blockingQueue.offer("A");
        blockingQueue.offer("B");
        blockingQueue.offer("C");
        // 三个参数是 ： 插入元素 ，超时时间 ，时间单位
        //这个意思是 ：插入D 元素，如果等待超过两秒，就退出程序
        //blockingQueue.offer("D",2, TimeUnit.SECONDS);
        System.out.println("==================");
        blockingQueue.poll();
        blockingQueue.poll();
        System.out.println(blockingQueue.poll());
        //队空再取，等待超过两秒就退出
        blockingQueue.poll(2,TimeUnit.SECONDS);

    }
}
