package com.JUC.add;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @Author Feng
 * @Date 2021/10/21 15:04
 * @Version 1.0
 * @Description  线程辅助类 :加法计数器
 */
public class CyclicBarrierTest {
    public static void main(String[] args) {
        /*
        * 举个例子 ： 集齐七颗龙珠召唤 无敌憨憨
        *
        * */
        //召唤神龙的线程
        // CyclicBarrier构造方法有两个参数，一个是 计数的总数，一个是实现Runnable的类

        CyclicBarrier cyclicBarrier = new CyclicBarrier(8, () -> {
            System.out.println("召唤神龙！");
        });
        for (int i = 1; i <= 8; i++) {
            final int temp =i; //将for循环的局部变量 i ,变成常量复制进堆内存，这样内部类就可以使用
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+"收集了"+temp+"个龙珠");
                try {
                    //这玩意如果线程够的话，就执行屏障数 8，不够就不执行。但是只有满足为0时，才会唤醒神龙线程
                    cyclicBarrier.await();//等待，实际也是为计数器为0后，执行召唤神龙的线程
                    System.out.println("-1");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            },String.valueOf(i)).start();

        }
    }
}
