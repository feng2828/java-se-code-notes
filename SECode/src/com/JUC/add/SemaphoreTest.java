package com.JUC.add;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * @Author Feng
 * @Date 2021/10/21 15:37
 * @Version 1.0
 * @Description :线程辅助类 :信号量 ，就是操作系统中的PV操作
 */
public class SemaphoreTest {
    public static void main(String[] args) {
        // 这个参数是:许可证 – 可用许可证的初始数量，也就是可用资源的数目
        //这里比作3个停车位
        Semaphore semaphore = new Semaphore(3);
        //假设有6个车
        for (int i = 1; i <= 6; i++) {
            new Thread(() -> {
                //acquire() 得到  ，阻塞其
                //release() 释放 ，也就是将许可证数添加
                try {
                    semaphore.acquire();//得到
                    System.out.println(Thread.currentThread().getName()+"抢到了车位！");
                    TimeUnit.SECONDS.sleep(2);//睡两秒
                    System.out.println(Thread.currentThread().getName()+"走了");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    semaphore.release();//释放
                }
            },String.valueOf(i)).start();
        }
    }
}
