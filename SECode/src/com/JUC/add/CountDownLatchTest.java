package com.JUC.add;

import java.util.concurrent.CountDownLatch;

/**
 * @Author Feng
 * @Date 2021/10/21 14:40
 * @Version 1.0
 * @Description 线程辅助类 :减法计数器  类似于一个守护线程的减法计数器，当线程计数完成后进行结尾操作
 */
public class CountDownLatchTest {
    public static void main(String[] args) throws InterruptedException {
        //一般用于有些必须要执行的任务的时候。就使用这个！
        //它的参数就是计数的总数
        CountDownLatch countDownLatch = new CountDownLatch(6);
        for (int i = 1; i <= 6; i++) {  //这个循环的数得和计数器一样就是，相当于线程数和计数器一样个数，不然就失效
            new Thread(()->{
                countDownLatch.countDown();//这个代码的意思是 -1 ,就是计数器减一
                System.out.println(Thread.currentThread().getName()+"==>nice!");
            },String.valueOf(i)).start();
        }
        countDownLatch.await();//当计数器归零时，线程休眠，然后从这向下执行
        System.out.println("Close Door!");
    }
}
