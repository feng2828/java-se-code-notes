package com.JUC.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author Feng
 * @Date 2021/11/1 16:11
 * @Version 1.0
 * @Description Lock版的可重入锁
 */
public class Demo02 {
    public static void main(String[] args) {
        Phone2 phone = new Phone2();
        new Thread(() -> {
            phone.sms();
        }, "A").start();
        new Thread(() -> {
            phone.sms();
        }, "B").start();
    }
}


class Phone2 {

    Lock lock = new ReentrantLock();

    public void sms() {
        lock.lock();  // 这里加锁获得sms锁的时候其实也获得了下面call的锁，但是不会造成死锁，两个加锁的过程
        // 还有 上锁和解锁的操作必须配对，不然会死锁
        try {
            System.out.println(Thread.currentThread().getName() + "sms");
            call();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void call() {
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + "call");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}
