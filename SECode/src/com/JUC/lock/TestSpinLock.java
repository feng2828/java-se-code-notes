package com.JUC.lock;

import java.util.concurrent.TimeUnit;

/**
 * @Author Feng
 * @Date 2021/11/2 10:11
 * @Version 1.0
 * @Description 自旋锁测试类
 */
public class TestSpinLock {
    public static void main(String[] args) throws InterruptedException {

        SpinLockDemo lock = new SpinLockDemo();

        // 结果是 ： t1 ,t2都先上锁，然后t1获得锁，t2 不停的自旋，直到t1执行完后，t2才自旋结束

        new Thread(() -> {
            lock.myLock();
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.myUnLock();
            }
        }, "T1").start();


        TimeUnit.SECONDS.sleep(1);

        new Thread(() -> {
            lock.myLock();
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.myUnLock();
            }
        }, "T2").start();

        /*结果*/
//T1==>MyLock
//T2==>MyLock
//T1==>MyUnLock
//T2==>MyUnLock
    }
}
