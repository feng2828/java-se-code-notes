package com.JUC.lock;

import java.util.concurrent.atomic.AtomicReference;

/**
 * @Author Feng
 * @Date 2021/11/1 18:30
 * @Version 1.0
 * @Description 自旋锁
 */
public class SpinLockDemo {

    // int 默认是0
    // Thread 默认是 null
    AtomicReference<Thread> atomicReference = new AtomicReference();

    // 加锁
    public void myLock() {

        Thread thread = Thread.currentThread();
        System.out.println(Thread.currentThread().getName() + "==>MyLock");
        // 自旋锁 ： 一直进行原子性的操作循环，直到成功
        while (!atomicReference.compareAndSet(null, thread)) {

        }
    }

    // 解锁
    public void myUnLock() {

        Thread thread = Thread.currentThread();
        System.out.println(Thread.currentThread().getName() + "==>MyUnLock");
        // 自旋锁 ： 一直进行原子性的操作循环，直到成功
        atomicReference.compareAndSet(thread, null);
    }
}
