package com.JUC.lock;

import java.util.concurrent.TimeUnit;

/**
 * @Author Feng
 * @Date 2021/11/2 10:25
 * @Version 1.0
 * @Description 死锁测试
 */
public class DeadLockDemo {
    public static void main(String[] args) {
        String lockA="LockA";
        String lockB="LockB";

        new Thread(new MyThread(lockA,lockB),"T1").start();
        new Thread(new MyThread(lockB,lockA),"T2").start();
    }
}

class MyThread implements Runnable {


    private String lock1;
    private String lock2;

    public MyThread(String lock1, String lock2) {
        this.lock1 = lock1;
        this.lock2 = lock2;
    }


    @Override
    public void run() {
        synchronized (lock1){
            System.out.println(Thread.currentThread().getName()+"lock:"+lock1+"====>get"+lock2);
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (lock2){
                System.out.println(Thread.currentThread().getName()+"lock:"+lock2+"====>get"+lock1);
            }
        }
    }
}