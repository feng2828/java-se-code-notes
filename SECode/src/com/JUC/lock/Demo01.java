package com.JUC.lock;

/**
 * @Author Feng
 * @Date 2021/11/1 15:51
 * @Version 1.0
 * @Description synchronized版的可重入锁
 */
public class Demo01 {
    public static void main(String[] args) {
        Phone phone = new Phone();
        new Thread(() -> {
            phone.sms();
        }, "A").start();
        new Thread(() -> {
            phone.sms();
        }, "B").start();
    }
}

// 这里看似是 sms 包含了 call的锁，其实都是 this对象的同一把锁，看顺序执行，所以加上休眠时间进行改进
class Phone {

    public synchronized void sms() {
        System.out.println(Thread.currentThread().getName() + "sms");
        // 可以在这中间加上点休眠时间，用以证明已经获得了call锁
        // 可以证明即使A线程休眠也打印出 A call  ，说明获取 sms的锁时，自动可重入锁call
        try {
            Thread.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        call();
    }

    public synchronized void call() {
        System.out.println(Thread.currentThread().getName() + "call");

    }

}