package com.JUC.single;

/**
 * @Author Feng
 * @Date 2021/10/27 16:10
 * @Version 1.0
 * @Description 饿汉式单例 ：会在加载时就会初始化
 */
public class Hungry {

    //可能会浪费空间
    private byte[] data1=new byte[1024*1024];
    private byte[] data2=new byte[1024*1024];
    private byte[] data3=new byte[1024*1024];
    private byte[] data14=new byte[1024*1024];

    //单例构造器私有
    private Hungry() {

    }
    // 饿汉式就是类加载就把所有东西就加载进来了，消耗资源
    private final static Hungry hungry = new Hungry();

    public static Hungry getInstance(){
        return hungry;
    }
}
