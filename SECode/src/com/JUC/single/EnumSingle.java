package com.JUC.single;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @Author Feng
 * @Date 2021/10/27 19:24
 * @Version 1.0
 * @Description 枚举单例
 */

// 枚举是什么？ enum本身也是一个类,所有枚举类都默认是Enum类的子类，无需我们使用extends来继承。这说明Enum中的方法所有枚举类都拥有。
    // 枚举是其实就是多例,枚举项，它们都是本类的实例,例如下面的 INSTANCE 就是一个实例对象
    // 枚举类的构造器不可以添加访问修饰符，枚举类的构造器默认是private的,因为枚举类的实例不能让外界来创建！
public enum EnumSingle {

    INSTANCE;

    public EnumSingle getInstance(){
        return INSTANCE;
    }

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        // 枚举可以直接点
        EnumSingle instance1 = EnumSingle.INSTANCE;
        EnumSingle instance2 = EnumSingle.INSTANCE;
        System.out.println(instance1);
        System.out.println(instance2);
        //假设通过反射暴力获取实例
        Constructor<EnumSingle> declaredConstructor = EnumSingle.class.getDeclaredConstructor(String.class,int.class);
        declaredConstructor.setAccessible(true);
        EnumSingle instance3 = declaredConstructor.newInstance();

        // java.lang.NoSuchMethodException: com.JUC.single.EnumSingle.<init>()
        // 发现结果是没有找到构造方法，但是IDEA的out源码里有，为什么？
        // 我们在 class文件目录打开cmd进行反编译，发现还是有无参构造，再借助jad进行反编译，发现是有参构造
        System.out.println(instance3);

        // 经过修改反射获取构造方法的参数，我们最终得到正确结果
        // java.lang.IllegalArgumentException: Cannot reflectively create enum objects
        //反射不能创造枚举实例
    }


}
