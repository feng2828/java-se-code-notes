package com.JUC.single;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/**
 * @Author Feng
 * @Date 2021/10/27 16:22
 * @Version 1.0
 * @Description 懒汉式单例  ：只有在获取单例时才会初始化
 */
public class Lazy {

    //定义一个标志位,但是又可以通过反射破坏这个标志位
    private static boolean flag = false;

    private Lazy() {
        // 加锁防止反射破坏
        synchronized (Lazy.class) {
            if (flag == false) {
                flag = true;
            }
            if (lazy!=null){
                throw new RuntimeException("不要试图用反射破坏异常！");
            }
        }
        System.out.println(Thread.currentThread().getName() + "==>ok");
    }

    // volatile 保证指令重排导致的多线程问题
    private volatile static Lazy lazy;

    // 采用了一个double check机制，就是当如果有实例的话，只有在创建实例的时候才会抢夺锁，其他线程
    // 仅仅一个读取的操作都需要等待锁，会造成效率低下的问题，所以说锁的范围越小越好

    // 双重检测锁模式的懒汉式单例 简称 DCL懒汉式
    // 这个加锁是为了保证操作原子性，只有一个线程能创建一个实例，其他线程不能创建实例了
    private static Lazy getInstance() {
        if (lazy == null) {
            // 锁当前类对象
            synchronized (Lazy.class) {
                if (lazy == null) {
                    /**
                     * 这个创建实例的操作
                     * 1. 分配内存空间
                     * 2. 执行构造方法，初始化对象
                     * 3. 把这个对象指向这个空间。
                     *
                     * 由于指令重排，多线程情况下，可能会导致其他线程初始化对象时，已经有对象指向这个空间
                     * 然后线程并没有完成构造，导致lazy不是空的，所以要加 volatile
                     *
                     */
                    lazy = new Lazy(); // 不是一个原子性操作 ，
                }
            }
        }
        return lazy;
    }

    // 单线程下 ，这个单例没有问题

    // 多线程并发情况下,没加锁每次结果都不一样，证明有问题
    /*public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                Lazy.getInstance();
            }).start();
        }
    }*/

    // 反射获取实例
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {
        //Lazy instance1 = Lazy.getInstance();
        Field flag = Lazy.class.getDeclaredField("flag");//得到字段
        flag.setAccessible(true);
        //得到全部的构造器，包括私有的构造器
        Constructor<Lazy> declaredConstructor = Lazy.class.getDeclaredConstructor(null);
        // 暴力反射，设置可见性可见
        declaredConstructor.setAccessible(true);
        //创建对象实例
        Lazy instance2 = declaredConstructor.newInstance();
        // 假如这里又新建一个实例，那么构造方法里的锁又没用了,就要使用标志位了
        //flag.set(instance2,false);
        Lazy instance3 = declaredConstructor.newInstance();
        System.out.println(instance2.hashCode() == instance3.hashCode());
        //System.out.println(instance1.hashCode() == instance2.hashCode()); // false
        // 这里证明了 反射会破坏单例模式
    }
}
