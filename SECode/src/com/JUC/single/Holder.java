package com.JUC.single;

/**
 * @Author Feng
 * @Date 2021/10/27 16:58
 * @Version 1.0
 * @Description 静态内部类实现单例
 */
public class Holder {
    private Holder(){

    }

    public static Holder getInstance(){
        return InnerClass.holder;
    }

    public static class InnerClass{
        private final static Holder holder = new Holder();
    }


}
