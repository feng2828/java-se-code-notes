package com.JUC.volatiledemo;

import java.util.concurrent.TimeUnit;

/**
 * @Author Feng
 * @Date 2021/10/27 9:08
 * @Version 1.0
 * @Description JMM 测试类
 */
public class JMMDemo {
    //这里有两个线程 ： 主线程和A线程

    // volatile 保证可见性
    private volatile static int num=0; // 不加 volatile 程序就会死循环
    public static void main(String[] args) {
        new Thread(()->{ // 线程A对主存的变化是不知道的
            while (num==0){

            }
        },"A").start();

        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        num=1;
        System.out.println(num); // 没有volatile时 结果是1，但是线程一直在执行
        // 因为主线程修改了主存中的值，而在A线程中的工作内存 num还是0，所以一直处在循环中
        // 加上之后就不会一直死循环了
    }
}
