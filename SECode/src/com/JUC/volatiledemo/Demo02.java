package com.JUC.volatiledemo;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author Feng
 * @Date 2021/10/27 10:44
 * @Version 1.0
 * @Description 不保证原子性测试
 *
 *
 * 解决方式 ：1.在同步方法上加锁 synchronized 或者 lock ,但是会降低效率
 */
public class Demo02 {

    // 这里加不加 volatile 结果不变 没用
    //private volatile static int num = 0;

    // 我们使用原子类来保证 原子性
    private  static AtomicInteger num = new AtomicInteger();// 原子类的Integer

    // synchronized
    public  static void add() {
        // num++;//不是一个原子性操作
        num.getAndIncrement(); // AtomicInteger的 +1操作 ，底层用的是CAS
    }

    public static void main(String[] args) {
        // 20个线程，每个线程执行1000次add
        // 理论上num最后会等于 20*1000 =20000
        for (int i = 0; i < 20; i++) {
            new Thread(() -> {
                for (int i1 = 0; i1 < 1000; i1++) {
                    add();
                }
            }).start();
        }
        while (Thread.activeCount() > 2) {// 最基本的两个 main 和 GC
            // 还有线程在的时候，主线程礼让
            Thread.yield();
        }
        // 结果是 main=>18406  ，不符合预期，因为线程的原子性导致有些线程失败了
        System.out.println(Thread.currentThread().getName() + "=>" + num);
    }
}
