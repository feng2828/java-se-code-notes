package com.JUC.function;

import java.util.function.Consumer;

/**
 * @Author Feng
 * @Date 2021/10/25 18:11
 * @Version 1.0
 * @Description Consumer测试类
 * <p>
 * Consumer：消费型接口 ，只有输入，没有返回值
 */
public class Demo03 {
    public static void main(String[] args) {
     /*   Consumer<String> consumer= new Consumer<String >() {
            @Override
            public void accept(String str) {
                System.out.println(str);
            }
        };*/
        Consumer<String> consumer = (str) -> {
            System.out.println(str);
        };
        consumer.accept("哈哈哈！");
    }
}
