package com.JUC.function;

import java.util.function.Predicate;

/**
 * @Author Feng
 * @Date 2021/10/25 16:49
 * @Version 1.0
 * @Description Predicate测试类
 * <p>
 * 断定型接口 ：有一个输入参数,返回值
 */
public class Demo02 {
    public static void main(String[] args) {
        // 比如这里可以用来判断字符串是否为空
//        Predicate<String> predicate = new Predicate<String>() {
//            @Override
//            public boolean test(String str) {
//                return (str == null || str.isEmpty());
//            }
//        };
        Predicate<String> predicate = (str) -> {
            return (str == null || str.isEmpty());
        };
        System.out.println(predicate.test(""));//true
    }
}
