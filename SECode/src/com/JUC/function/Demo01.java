package com.JUC.function;

import java.util.function.Function;

/**
 * @Author Feng
 * @Date 2021/10/25 16:12
 * @Version 1.0
 * @Description Function测试类
 *
 * Function函数型接口 ：有一个输入参数，有一个输出
 *  只要是函数式接口，我们就可以用 Lambda表达式简化
 */
public class Demo01 {
    public static void main(String[] args) {
        // 工具类 ：输出输入的值
      /*  Function function=new Function<String,String>() {
            @Override
            public String apply(String str) {
                return str;
            }
        };*/
        Function function=(str)->{
            return str;
        };
        System.out.println(function.apply("agg"));
    }
}
