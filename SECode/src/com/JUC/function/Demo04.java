package com.JUC.function;

import java.util.function.Supplier;

/**
 * @Author Feng
 * @Date 2021/10/25 18:11
 * @Version 1.0
 * @Description Supplier 测试类
 * Supplier供给型接口 ：没有参数，只有返回值
 */
public class Demo04 {
    public static void main(String[] args) {
        //这个泛型参数限定的是返回值类型
//        Supplier<String> supplier =  new Supplier<String>() {
//            @Override
//            public String get() {
//                return "nice!";
//            }
//        };
        Supplier<String> supplier = ()->{
            return "nice";
        };
        System.out.println(supplier.get());
    }
}
