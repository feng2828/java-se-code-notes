package com.Reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @Author Feng
 * @Date 2021/9/5 21:32
 * @Version 1.0
 * @Describe 获取类的信息
 */
public class Test08 {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException, NoSuchMethodException {
        Class<?> a = Class.forName("com.Reflection.User");

        //获取名字
        System.out.println(a.getName());//com.Reflection.User
        System.out.println(a.getSimpleName());//User

        //获取类的属性
        Field[] fields1 = a.getFields();//只能找到public的属性
        for (Field field : fields1) {
            System.out.println(field);//public int com.Reflection.User.id
        }
        Field[] fields = a.getDeclaredFields();//这个是所有的
        for (Field field : fields) {
            System.out.println(field);
            //private java.lang.String com.Reflection.User.name
            //public int com.Reflection.User.id
            //private int com.Reflection.User.age
        }
        Field id = a.getField("id");//获取指定的公共属性，私有加上getDeclaredFields
        System.out.println(id);//public int com.Reflection.User.id

        System.out.println("====================");
        //获取类的方法
        Method[] methods = a.getMethods();//获得本类及其父类的所有public 方法
        for (Method method : methods) {
            System.out.println(method);
        }
        System.out.println("===============");
        methods=a.getDeclaredMethods();//获得本类的所有方法
        for (Method method : methods) {
            System.out.println(method);
        }
        System.out.println(a.getDeclaredMethod("getId",null));//获取指定方法
        System.out.println(a.getDeclaredMethod("setName",String.class));
        //获取的方法是带参数时，要指定类型的class对象,防止重载方法名一样时，带参能判断

        //获取构造器
        System.out.println("======================");
        Constructor<?>[] constructors = a.getConstructors();//获取所有的public构造器
        for (Constructor<?> constructor : constructors) {
            System.out.println(constructor);
            //public com.Reflection.User()
            // public com.Reflection.User(java.lang.String,int,int)
        }
        constructors = a.getDeclaredConstructors();//获取所有的构造器
        for (Constructor<?> constructor : constructors) {
            System.out.println(constructor);
            //public com.Reflection.User()
            // public com.Reflection.User(java.lang.String,int,int)
        }
        //获取指定的构造器
        Constructor<?> constructor = a.getDeclaredConstructor(String.class,int.class,int.class);
        System.out.println(constructor);//public com.Reflection.User(java.lang.String,int,int)
    }
}
