package com.Reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @Author Feng
 * @Date 2021/9/6 8:18
 * @Version 1.0
 * @Describe 分析性能问题
 */
public class Test10 {
    //普通方式调用
    public static void test01(){
        User user = new User();

        long start=System.currentTimeMillis();

        for (int i = 0; i < 1000000000; i++) {
            user.getName();
        }
        long end=System.currentTimeMillis();
        System.out.println("普通方式十亿次需要"+(end-start)+"ms");//普通方式十亿次需要3ms
    }


    //反射方式调用
    public static void test02() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        User user = new User();
        Class<? extends User> c1 = user.getClass();
        Method getName = c1.getDeclaredMethod("getName", null);

        long start=System.currentTimeMillis();

        for (int i = 0; i < 1000000000; i++) {
            getName.invoke(user,null);
        }
        long end=System.currentTimeMillis();
        System.out.println("反射方式十亿次需要"+(end-start)+"ms");//反射方式十亿次需要2120ms
    }


    //反射方式调用  关闭检测
    public static void test03() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        User user = new User();
        Class<? extends User> c1 = user.getClass();
        Method getName = c1.getDeclaredMethod("getName", null);
        getName.setAccessible(true);

        long start=System.currentTimeMillis();

        for (int i = 0; i < 1000000000; i++) {
            getName.invoke(user,null);
        }
        long end=System.currentTimeMillis();
        System.out.println("关闭检测方式十亿次需要"+(end-start)+"ms");//关闭检测方式十亿次需要1036ms
    }

    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, IllegalAccessException {
        test01();
        test02();
        test03();
    }
}
