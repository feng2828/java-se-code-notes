package com.Reflection;

import java.lang.annotation.ElementType;

/**
 * @Author Feng
 * @Date 2021/9/5 14:35
 * @Version 1.0
 * @Describe 所有类型的Class
 */
public class AllClass {
    public static void main(String[] args) {

        Class<Object> c1 = Object.class;//类的Class
        Class<Comparable> c2 = Comparable.class;//接口
        Class<String[]> c3 = String[].class;//数组
        Class<String[][]> c4 = String[][].class;//二维数组
        Class<Override> c5 = Override.class;//注解
        Class<ElementType> c6 = ElementType.class;//枚举
        Class<Integer> c7 = Integer.class;//基本数据类型
        Class<Void> c8 = void.class;//void
        Class<Class> c9 = Class.class;//Class本身

        System.out.println(c1);//class java.lang.Object
        System.out.println(c2);//interface java.lang.Comparable
        System.out.println(c3);//class [Ljava.lang.String;
        System.out.println(c4);//class [[Ljava.lang.String;
        System.out.println(c5);//interface java.lang.Override
        System.out.println(c6);//class java.lang.annotation.ElementType
        System.out.println(c7);//class java.lang.Integer
        System.out.println(c8);//void
        System.out.println(c9);//class java.lang.Class

        //只要元素类型与维度一样，就是同一个Class
        int[] a=new int[10];
        int[] b=new int[102];
        System.out.println(a.getClass().hashCode());//460141958
        System.out.println(b.getClass().hashCode());//460141958
        //注意：1、如果两个对象equals相等，那么这两个对象的HashCode一定也相同
        //2、如果两个对象的HashCode相同，不代表两个对象就相同，只能说明这两个对象在散列存储结构中，存放于同一个位置

    }
}
class n{
    public static void main(String[] args) {
        n n = new n();
        n n2 = new n();
        System.out.println(n.getClass().hashCode());
        System.out.println(n2.getClass().hashCode());
    }
}