package com.Reflection;

/**
 * @Author Feng
 * @Date 2021/9/5 12:17
 * @Version 1.0
 */
//测试创建Class实例方法
public class CreateClass {
    public static void main(String[] args) throws ClassNotFoundException {
        Person person = new Student();
        System.out.println("这个人是:"+person.name);//这个人是:学生

        //方式一：通过对象获得
        Class c1 = person.getClass();
        System.out.println(c1.hashCode());//460141958
        //方式二：通过forName方式获取
        Class c2 = Class.forName("com.Reflection.Student");
        System.out.println(c2.hashCode());//460141958
        //方式三：通过 .class 获取
        Class c3 = Student.class;
        System.out.println(c3.hashCode());//460141958
        //方式四 ：基本内置类型的包装类都有一个TYPE属性
        Class c4 = Integer.TYPE;
        System.out.println(c4);//int
        //获得父类类型
        Class c5 = c2.getSuperclass();
        System.out.println(c5);//class com.Reflection.Person
    }
}
class Person{
    String name;

    public Person() {
    }

    public Person(String name) {
        this.name = name;
    }
}

class Student extends Person{
    public Student() {
        this.name="学生";
    }
}
class Teacher extends Person{
    public Teacher() {
        this.name="老师";
    }
}