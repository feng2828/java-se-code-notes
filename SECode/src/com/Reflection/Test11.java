package com.Reflection;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * @Author Feng
 * @Date 2021/9/6 9:13
 * @Version 1.0
 * @Describe 通过反射获取泛型
 */
public class Test11 {
    public void test01(Map<String,User> map, List<User> list){
        System.out.println("Tests01");
    }
    public Map<String,User> test02(){
        System.out.println("Test02");
        return null;
    }

    public static void main(String[] args) throws NoSuchMethodException {
        Method method = Test11.class.getDeclaredMethod("test01", Map.class, List.class);
        //获得参数的泛型类型，也就是 map和list两个参数的泛型类型
        Type[] genericParameterTypes = method.getGenericParameterTypes();
        for (Type genericParameterType : genericParameterTypes) {
            System.out.println(genericParameterType);
            //结果：java.util.Map<java.lang.String, com.Reflection.User>
            //java.util.List<com.Reflection.User>

            //判断泛型参数类型是否是结构化参数化类型，也就是泛型的参数是否是一种基本类型组成的
            if(genericParameterType instanceof ParameterizedType){
                //强转为结构化类型，再得到真实类型，就是String，User
                Type[] actualTypeArguments = ((ParameterizedType) genericParameterType).getActualTypeArguments();
                for (Type actualTypeArgument : actualTypeArguments) {
                    System.out.println(actualTypeArgument);
                    //结果
                    //java.util.Map<java.lang.String, com.Reflection.User>
                    //class java.lang.String
                    //class com.Reflection.User
                    //java.util.List<com.Reflection.User>
                    //class com.Reflection.User
                }
            }
        }
        System.out.println("==============================================");
        Method method2 = Test11.class.getDeclaredMethod("test02");
        Type genericReturnType = method2.getGenericReturnType();
        if(genericReturnType instanceof ParameterizedType) {
            //强转为结构化类型，再得到真实类型，就是String，User
            Type[] actualTypeArguments = ((ParameterizedType) genericReturnType).getActualTypeArguments();
            for (Type actualTypeArgument : actualTypeArguments) {
                System.out.println(actualTypeArgument);
                //class java.lang.String
                //class com.Reflection.User
            }
        }

    }
}
