package com.Reflection;

/**
 * @Author Feng
 * @Date 2021/9/5 15:28
 * @Version 1.0
 * @Describe 类加载
 */
public class ClassLoaderTest {
    public static void main(String[] args) {
        A a = new A();
        System.out.println(a.m);
        /**
         * 1.加载到内存，会产生一个类Class对象
         * 2.链接,链接结束后m = 0
         * 3.初始化
         *          <clinit>(){
         *              system.out.println ( "A类静态代码块初始化");
         *              m= 100;
         *              m=300
         *           }
         */
    }
}
class A{
    static int m=100;//静态变量
    //静态代码块
    static {
        System.out.println("A类静态代码块初始化");
        m=300;
    }


    public A() {
        System.out.println("A类的无参构造初始化！");
    }

}