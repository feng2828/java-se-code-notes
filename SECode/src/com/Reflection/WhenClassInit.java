package com.Reflection;

/**
 * @Author Feng
 * @Date 2021/9/5 15:56
 * @Version 1.0
 * @Describe 测试类什么时候会初始化
 */
public class WhenClassInit {
    static {
        System.out.println("main类被加载！");
    }

    public static void main(String[] args) throws ClassNotFoundException {
        //1.主动引用
        //Son son = new Son();//main类被加载！  父类被加载！  子类被加载！
        //2.反射也会主动引用
        //Class son = Class.forName("com.Reflection.Son");

        //不会产生类的引用
        //1.子类引用父类的静态变量
        //System.out.println(Son.b);  //main类被加载！ 父类被加载！ 2
        //2.数组的定义,只是开辟一个空间
        // Son[] arry=new Son[5]; //main类被加载！
        //3.引用常量
        System.out.println(Son.M); //main类被加载！  1


    }
}
class Father{
    static int b=2;
    static {
        System.out.println("父类被加载！");
    }
}
class Son extends Father{
    static {
        System.out.println("子类被加载！");
        m=300;
    }
    static int m=100;
    static final int M=1;
}