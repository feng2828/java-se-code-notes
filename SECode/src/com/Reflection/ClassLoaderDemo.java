package com.Reflection;

/**
 * @Author Feng
 * @Date 2021/9/5 20:59
 * @Version 1.0
 * @Describe 类加载器的获取和使用
 */
public class ClassLoaderDemo {
    public static void main(String[] args) throws ClassNotFoundException {
        //获取系统类加载器
        ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
        System.out.println(systemClassLoader);//sun.misc.Launcher$AppClassLoader@18b4aac2
        //获取系统类加载器的父类--->扩展类加载器
        ClassLoader parent = systemClassLoader.getParent();
        System.out.println(parent);//sun.misc.Launcher$ExtClassLoader@1b6d3586
        //获取扩展类加载器的父类---> 根加载器（引导类加载器，c/c++写的,java输出不出来）
        ClassLoader parent1 = parent.getParent();
        System.out.println(parent1);//null

        //测试当前类是哪个加载器加载的
        Class<?> a = Class.forName("com.Reflection.ClassLoaderDemo");
        //是系统类加载器
        System.out.println(a.getClassLoader());//sun.misc.Launcher$AppClassLoader@18b4aac2

        //测试JDK内置类是谁加载的
        ClassLoader loader = Class.forName("java.lang.Object").getClassLoader();
        System.out.println(loader);//null  :是根加载器

        //双亲委派机制 ：保证安全性
        //根加载器的优先级最高，依次向下
        //比如你自己写了一个 java.lang.String的类，但是运行后他不会用你的会用更高级的

        //如何获取系统类加载器可以加载的路径
        System.out.println(System.getProperty("java.class.path"));
        /** 结果如下
         * D:\jdk8\1.8.261\jre\lib\charsets.jar;
         * D:\jdk8\1.8.261\jre\lib\deploy.jar;
         * D:\jdk8\1.8.261\jre\lib\ext\access-bridge-64.jar;
         * D:\jdk8\1.8.261\jre\lib\ext\cldrdata.jar;
         * D:\jdk8\1.8.261\jre\lib\ext\dnsns.jar;
         * D:\jdk8\1.8.261\jre\lib\ext\jaccess.jar;
         * D:\jdk8\1.8.261\jre\lib\ext\jfxrt.jar;
         * D:\jdk8\1.8.261\jre\lib\ext\localedata.jar;
         * D:\jdk8\1.8.261\jre\lib\ext\nashorn.jar;
         * D:\jdk8\1.8.261\jre\lib\ext\sunec.jar;
         * D:\jdk8\1.8.261\jre\lib\ext\sunjce_provider.jar;
         * D:\jdk8\1.8.261\jre\lib\ext\sunmscapi.jar;
         * D:\jdk8\1.8.261\jre\lib\ext\sunpkcs11.jar;
         * D:\jdk8\1.8.261\jre\lib\ext\zipfs.jar;
         * D:\jdk8\1.8.261\jre\lib\javaws.jar;
         * D:\jdk8\1.8.261\jre\lib\jce.jar;
         * D:\jdk8\1.8.261\jre\lib\jfr.jar;
         * D:\jdk8\1.8.261\jre\lib\jfxswt.jar;
         * D:\jdk8\1.8.261\jre\lib\jsse.jar;
         * D:\jdk8\1.8.261\jre\lib\management-agent.jar;
         * D:\jdk8\1.8.261\jre\lib\plugin.jar;
         * D:\jdk8\1.8.261\jre\lib\resources.jar;
         * D:\jdk8\1.8.261\jre\lib\rt.jar;
         * D:\IDEA\Projects\JavaSE\JavaSEDemo\out\production\SECode;
         * D:\IDEA\IntelliJ IDEA 2020.2.4\lib\idea_rt.jar
         */

    }
}
