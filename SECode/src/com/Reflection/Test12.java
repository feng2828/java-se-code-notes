package com.Reflection;

import java.lang.annotation.*;
import java.lang.reflect.Field;

/**
 * @Author Feng
 * @Date 2021/9/6 9:41
 * @Version 1.0
 * @Describe 练习反射操作注解
 */
public class Test12 {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException {
        Class<?> c1 = Class.forName("com.Reflection.Student02");
        //通过反射获取注解
        Annotation[] annotations = c1.getAnnotations();//获得类注解
        for (Annotation annotation : annotations) {
            System.out.println(annotation);//@com.Reflection.TableFeng(value=db_student)
        }
        //获得注解的value值
        TableFeng annotation = c1.getAnnotation(TableFeng.class);
        System.out.println(annotation.value());//db_student

        //获得字段的指定注解
        Field id = c1.getDeclaredField("id");//先获得字段,注意是getDeclaredField，不然私有字段获取不出来
        FieldFeng fieldFeng = id.getAnnotation(FieldFeng.class);
        System.out.println(fieldFeng.columnName());//db_id
        System.out.println(fieldFeng.type());//int
        System.out.println(fieldFeng.length());//5
    }

}
@TableFeng("db_student")
class Student02{
    @FieldFeng(columnName = "db_id",type = "int",length = 5)
    private int id;
    @FieldFeng(columnName = "db_name",type = "varchar",length = 10)
    private String name;
    @FieldFeng(columnName = "db_age",type = "int",length = 5)
    private int age;

    public Student02() {
    }

    public Student02(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student02{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

//自定义类名注解
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@interface TableFeng{
    String value();
}

//自定义属性注解
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@interface FieldFeng{
    String columnName();//字段名
    String type();//类型
    int length();//长度
}