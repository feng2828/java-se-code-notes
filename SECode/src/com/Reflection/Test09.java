package com.Reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @Author Feng
 * @Date 2021/9/5 22:29
 * @Version 1.0
 * @Describe 通过反射,动态的创建对象，
 */
public class Test09 {
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, NoSuchFieldException {
        Class<?> c1 = Class.forName("com.Reflection.User");

        //构造一个对象
//        User user1 = (User) c1.newInstance();//本质是调用了类的无参构造器,删除了无参构造就报错
//        System.out.println(user1);//User{name='null', id=0, age=0}

        //通过构造器创建对象
//        Constructor<?> constructor = c1.getDeclaredConstructor(String.class, int.class, int.class);
//        User user2 = (User) constructor.newInstance("你好", 2, 3);
//        System.out.println(user2);//User{name='null', id=0, age=0}

        //通过反射调用普通方法
        User user3 = (User) c1.newInstance();
        //通过反射获取特定的方法
        Method setName = c1.getDeclaredMethod("setName", String.class);
        //为啥不直接通过对象调用setName（）方法，因为直接调用相当于直接写死name,不能这样的动态修改值
        setName.invoke(user3,"hhhh");//执行方法,参数一个是类对象。一个是传入参数
        System.out.println(user3.getName());//结果: hhhh

        //通过反射操作属性
        User user4 = (User) c1.newInstance();
        Field name = c1.getDeclaredField("name");
        //不能直接操作私有属性，所以要关掉权限检测
        name.setAccessible(true);
        name.set(user4,"你哈");
        System.out.println(user4.getName());//你哈

    }
}
