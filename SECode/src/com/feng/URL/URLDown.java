package com.feng.URL;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class URLDown {
    public static void main(String[] args) throws IOException {
        //1.下载地址
        URL url = new URL("http://tiebapic.baidu.com/forum/w%3D580%3B/sign=5c780329b236afc30e0c3f6d8322eac4/b999a9014c086e0642a271a247087bf40bd1cbd5.jpg");
        //2.连接到这个资源,通过URL打开连接，转换成http的URl连接
        HttpURLConnection hrc = (HttpURLConnection) url.openConnection();
        InputStream is = hrc.getInputStream();

        FileOutputStream fileOutputStream = new FileOutputStream("hh.jpg");
        byte[] bytes = new byte[1024];
        int len;
        while ((len = is.read(bytes)) != -1) {
            fileOutputStream.write(bytes, 0, len);
        }
        fileOutputStream.close();
        is.close();
        hrc.disconnect();//断开连接
    }
}
