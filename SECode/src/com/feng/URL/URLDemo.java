package com.feng.URL;

import java.net.MalformedURLException;
import java.net.URL;

public class URLDemo {
    public static void main(String[] args) throws MalformedURLException {
        URL url = new URL("https://www.baidu.com/");
        System.out.println(url.getProtocol());//获取协议
        System.out.println(url.getHost());//获取ip
    }
}
