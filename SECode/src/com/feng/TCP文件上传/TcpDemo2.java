package com.feng.TCP文件上传;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class TcpDemo2 {
    public static void main(String[] args) {
        Socket socket=null;
        OutputStream os=null;
        FileInputStream fis=null;
        try {
            InetAddress ServerIP = InetAddress.getByName("127.0.0.1");
            int port=9000;
            //通过Socket来创建连接
            socket = new Socket(ServerIP, port);
            //通过流来输出
            os = socket.getOutputStream();
            //文件流
            //读取
            fis = new FileInputStream(new File("1.jpg"));
            //写出文件
            byte[] bytes = new byte[1024];
            int len;
            while ((len= fis.read(bytes))!=-1){
                os.write(bytes,0,len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            //通知服务器，我已经结束
            try {
                socket.shutdownOutput();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //要确定服务端接受完毕才关闭连接
            try {
                InputStream inputStream = socket.getInputStream();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] bytes2 = new byte[1024];
                int len2;
                while ((len2= inputStream.read(bytes2))!=-1){
                    baos.write(bytes2,0,len2);
                }
                System.out.println(baos.toString());
                baos.close();
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(fis!=null){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(os!=null){
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(socket!=null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
