package com.feng.TCP文件上传;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class TcpServerDemo2 {
    public static void main(String[] args) {
        ServerSocket serverSocket = null;
        Socket socket=null;
        InputStream is =null;
        FileOutputStream fos=null;
        try {
            serverSocket = new ServerSocket(9000);
            socket = serverSocket.accept();//接收连接
            is = socket.getInputStream();

            fos = new FileOutputStream(new File("2.jpg"));
            byte[] bytes = new byte[1024];
            int len;
            while ((len= is.read(bytes))!=-1){
                fos.write(bytes,0,len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            //通知客户端接收完毕
            OutputStream outputStream = null;
            try {
                outputStream = socket.getOutputStream();
                outputStream.write("我接受完毕了".getBytes());
                outputStream.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
            if(fos!=null){
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(is!=null){
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(socket!=null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(serverSocket!=null){
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
