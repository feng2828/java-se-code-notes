package com.feng.UDPChat;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;

public class UdpSenderDemo01 {
    public static void main(String[] args) throws IOException {
        DatagramSocket socket = new DatagramSocket(8888);

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true){
            String datas = reader.readLine();
            DatagramPacket packet = new DatagramPacket
                    (datas.getBytes(),
                            0,
                            datas.getBytes().length,
                            new InetSocketAddress("127.0.0.1",6666));
            socket.send(packet);
            if(datas.equals("bye")){
                break;
            }
        }
        socket.close();
    }
}
