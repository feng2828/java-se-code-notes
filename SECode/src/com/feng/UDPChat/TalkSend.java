package com.feng.UDPChat;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;

public class TalkSend implements Runnable{
    DatagramSocket socket=null;
    BufferedReader reader=null;

    private int fromPort;//自身端口
    private String toIP;//发送对方ip
    private int toPort;//发送方端口

    public TalkSend(int fromPort, String toIP, int toPort) {
        this.fromPort = fromPort;
        this.toIP = toIP;
        this.toPort = toPort;
        try {
            socket = new DatagramSocket(fromPort);
            reader = new BufferedReader(new InputStreamReader(System.in));
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    public TalkSend() {
    }

    @Override
    public void run() {
        while (true){
            try {
                String datas = reader.readLine();
                DatagramPacket packet = new DatagramPacket
                        (datas.getBytes(),
                                0,
                                datas.getBytes().length,
                                new InetSocketAddress(this.toIP,this.toPort));
                socket.send(packet);
                if(datas.equals("bye")){
                    break;
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        socket.close();
    }

}
