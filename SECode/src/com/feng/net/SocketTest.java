package com.feng.net;

import java.net.InetAddress;
import java.net.InetSocketAddress;

//套接字
public class SocketTest {
    public static void main(String[] args) {
        InetSocketAddress socketAddress = new InetSocketAddress("127.0.0.1", 6656);

        InetAddress address = socketAddress.getAddress();
        System.out.println(address);//获取地址
        System.out.println(socketAddress.getHostName());//获取主机名
        System.out.println(socketAddress.getPort());//获取端口
    }
}
