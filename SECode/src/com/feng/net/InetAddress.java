package com.feng.net;

import java.net.UnknownHostException;

//测试ip
public class InetAddress {
    public static void main(String[] args) {
        try {
            java.net.InetAddress name = java.net.InetAddress.getByName("www.baidu.com");
            System.out.println(name);
            System.out.println(name.getHostAddress());//获取ip
            System.out.println(name.getHostName());//获取域名
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
