package com.feng.UDP;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class UDPClientDemo1 {
    public static void main(String[] args) {
        //建立数据报socket连接
        DatagramSocket socket = null;
        try {
            socket = new DatagramSocket();

            //建立一个数据包
            String msg = "你好吖！";
            InetAddress ip = InetAddress.getByName("127.0.0.1");
            DatagramPacket dp = new DatagramPacket(msg.getBytes(), 0, msg.getBytes().length, ip, 9090);
            //发送包
            socket.send(dp);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            socket.close();
        }
    }
}
