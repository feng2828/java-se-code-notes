package com.feng.TCP;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class TcpServerDemo {
    public static void main(String[] args) {
        ServerSocket serverSocket = null;
        Socket socket=null;
        InputStream is =null;
        ByteArrayOutputStream bos = null;
        try {
            serverSocket = new ServerSocket(9999);
            socket = serverSocket.accept();//接收连接
            is = socket.getInputStream();
            //使用管道流来接受，管道流就是相当于服务器的过滤器，把所有的信息都先接受，防止乱码
            bos = new ByteArrayOutputStream();
            byte[] bytes = new byte[1024];
            int len;
            while ((len= is.read(bytes))!=-1){
                bos.write(bytes,0,len);
                System.out.println(bos.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(bos!=null){
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(is!=null){
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(socket!=null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(serverSocket!=null){
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
