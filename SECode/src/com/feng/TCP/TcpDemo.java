package com.feng.TCP;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class TcpDemo {
    public static void main(String[] args) {
        Socket socket=null;
        OutputStream os=null;
        try {
            InetAddress ServerIP = InetAddress.getByName("127.0.0.1");
            int port=9999;
            //通过Socket来创建连接
            socket = new Socket(ServerIP, port);
            //通过流来输出
            os = socket.getOutputStream();
            os.write("你好，小怪！".getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(os!=null){
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(socket!=null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
