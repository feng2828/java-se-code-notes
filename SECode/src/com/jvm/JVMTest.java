package com.jvm;

/**
 * @Author Feng
 * @Date 2021/9/20 9:38
 * @Version 1.0
 * @Describe
 */
public class JVMTest {
    public static void main(String[] args) {
        //虚拟机可获取的最大内存
        long maxMemory = Runtime.getRuntime().maxMemory();//字节  1024*1024
        //jvm初始化的内存
        long total = Runtime.getRuntime().totalMemory();
        System.out.println(maxMemory/1024/1024);
        System.out.println("==============");
        System.out.println(total/1024/1024);
    }
}
