package com.jvm;

import java.util.ArrayList;

/**
 * @Author Feng
 * @Date 2021/9/20 10:24
 * @Version 1.0
 * @Describe
 */

//-Xms:设置初始化内存分配大小 默认 1/64
//-Xmx :设置最大分配内存  默认 1/4
// -XX:+PrintGCDetails   打印GC的具体信息
// -XX:+HeapDumpOnOutOfMemoryError  打印堆溢出 OOMDump

//-Xms1m -Xmx8m -XX:+HeapDumpOnOutOfMemoryError
/*public class OOMError2 {
   byte[] array= new byte[1*1024*1024];// 1M

    public static void main(String[] args) {
        ArrayList<OOMError2> list = new ArrayList<>();
        int count=0;
        try {
            while (true){
                list.add(new OOMError2());
                count=count+1;
            }
        }catch (Exception e){
            System.out.println("OOM");
            e.printStackTrace();
        }
    }
}*/
