package com.ProxyStatic.test;

public class StaticProxy {
    public static void main(String[] args) {
        WeddingCompany weddingCompany = new WeddingCompany(new You());
        weddingCompany.HappyMarry();
    }
}

//结婚
interface Marry {
    void HappyMarry();
}

//真实角色
class You implements Marry {
    @Override
    public void HappyMarry() {
        System.out.println("我结婚了");
    }
}

//代理对象
class WeddingCompany implements Marry {
    private Marry target;//代理目标对象

    public WeddingCompany(Marry target) {
        this.target = target;
    }

    @Override
    public void HappyMarry() {
        before();
        this.target.HappyMarry();
        after();
    }

    private void before() {
        System.out.println("结婚前");
    }

    private void after() {
        System.out.println("结婚后");
    }
}
