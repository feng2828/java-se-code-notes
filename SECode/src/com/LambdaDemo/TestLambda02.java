package com.LambdaDemo;

public class TestLambda02 {
    public static void main(String[] args) {
        Love love=(int a)-> System.out.println("I love you"+a);//带参的
        love.love(2);
        //进一步简化
        //前提是接口是函数式接口
        //参数是一个是可以省略参数类型和括号，多个参数也可以去掉参数类型，要去掉就都去掉,但是必须加上括号
        // 方法体只有一条语句时，才可以省略大括号
        love=a -> System.out.println("I love you3"+a);
        love.love(3);

    }
}
interface Love{
    void love(int a);
}
