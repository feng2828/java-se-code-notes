package com.LambdaDemo;

/**
 * 推倒Lambda表达式
 */
public class TestLambda01 {

    //3.静态内部类
    static class ILike2 implements Like{
        @Override
        public void lambda() {
            System.out.println("I like lambda2");
        }
    }
    public static void main(String[] args) {

        Like like=new ILike();
        like.lambda();


        like=new ILike2();
        like.lambda();

        //4.局部内部类
        class ILike3 implements Like{
            @Override
            public void lambda() {
                System.out.println("I like lambda3");
            }
        }
        like=new ILike3();
        like.lambda();

        //5.匿名内部类：没有类的名称，必须借助接口或者父类
        like=new Like() {
            @Override
            public void lambda() {
                System.out.println("I like lambda4");
            }
        };//这里注意分号，因为相当于这是一个语句
        like.lambda();

        //6.lambda简化,省略接口名和方法名
        like=()-> {
            System.out.println("I like lambda5");
        };
        like.lambda();


    }
}
//1.定义一个函数式接口哦
interface Like{
    void lambda();
}
//2.实现类
class ILike implements Like{
    @Override
    public void lambda() {
        System.out.println("I like lambda");
    }
}
