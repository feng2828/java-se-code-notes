package com.Thead.synchrinized;

import java.util.concurrent.CopyOnWriteArrayList;

//测试JUC安全类型的集合
//CopyOnWriteArrayList 这是并发包concurrent里的一个安全的集合
public class TestJUC {
    public static void main(String[] args) {
        CopyOnWriteArrayList<String> list = new CopyOnWriteArrayList<String>();
        for (int i = 0; i < 10000; i++) {
            new Thread(()->{
                list.add(Thread.currentThread().getName());
            }).start();
        }
        try {
            //为啥要睡，因为防止子线程没运行完，主线程就打印输出语句了
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(list.size());
    }
}
