package com.Thead.synchrinized;

//不安全的银行取钱
public class UnSafeBank {
    public static void main(String[] args) {
        //创建一个帐户
        Account nice = new Account(1000, "银行卡");
        Drawing you = new Drawing(nice, 50, "你");
        Drawing girl = new Drawing(nice, 100, "girl");
        you.start();
        girl.start();

    }
}

//帐户
class Account {
    int money;//余额
    String name;//卡号

    public Account(int money, String name) {
        this.money = money;
        this.name = name;
    }
}

//取款
class Drawing extends Thread {
    Account account;//帐户
    int drawMoney;//取了多少钱
    int nowMoney;//现在有多少钱

    public Drawing(Account account, int drawMoney, String name) {
        super(name);//给thead传name
        this.account = account;
        this.drawMoney=drawMoney;
    }
    //取钱操作
    @Override
    public void run() {
        //同步块锁账户
        synchronized (account){
            //判断有没有钱
            if(account.money-drawMoney<=0){
                System.out.println(Thread.currentThread().getName()+"没有钱！");
                return;
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //卡内余额
            account.money=account.money-drawMoney;
            //手里有的钱
            nowMoney=nowMoney+drawMoney;

            System.out.println(account.name+"余额为:"+account.money);
            System.out.println(this.getName()+"现在有:"+nowMoney);
            //  Thread.currentThread().getName()=this.getName()
            //因为当前类继承 Thead 所以当前对象的 getName()方法，也就是调用 Thead 的getName()方法
        }
    }
}

