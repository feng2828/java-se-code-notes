package com.Thead.synchrinized;
//不安全的线程买票
//线程不安全有，有负数 ，因为在最后一张票时，每个线程都把最后那张票的资源放在自己的工作内存中交互，
//没有排队，每个线程都去抢，内存控制不当造成数据不一致导致出现 0，-1，
public class UnSafeBuyTicket {
    public static void main(String[] args) {
        BuyTicket buyTicket = new BuyTicket();
        Thread thread01 = new Thread(buyTicket,"一号选手");
        Thread thread02 = new Thread(buyTicket,"二号选手");
        Thread thread03 = new Thread(buyTicket,"三号选手");
        thread01.start();
        thread02.start();
        thread03.start();
    }
}

class BuyTicket implements Runnable {

    private int TicketNums = 10;//票

    boolean flag=true;//线程停止标志位

    @Override
    public void run() {
        while (flag){
            buy();
        }
    }
    //synchronized 同步方法 :队列加锁  ，锁的是this 当前对象
    public synchronized void buy() {

        if (TicketNums <= 0) {
            flag=false;
            return;
        }
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + "买到了" + TicketNums--);
    }
}
