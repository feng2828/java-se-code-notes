package com.Thead.Lock.lock;

//生产者消费者模型====>利用缓冲区解决：管程法

//四个对象：生产者，消费者，产品，缓冲区
public class ProductTest {
    public static void main(String[] args) {
        SynContainer synContainer = new SynContainer();
        Productor productor = new Productor(synContainer);
        Consumer consumer = new Consumer(synContainer);
        productor.start();
        consumer.start();

    }

}

//1.生产者
class Productor extends Thread{
    
    SynContainer container;
    public Productor(SynContainer container) {
        this.container = container;
    }

    //生产
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            container.push(new Chicken(i));
            System.out.println("生产了"+i+"只鸡");
        }
    }
}
//2.消费者
class Consumer extends Thread{

    SynContainer container;
    public Consumer(SynContainer container) {
        this.container = container;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println("消费了===>"+container.pop().id+"只鸡");
        }
    }
}
//3.产品
class Chicken {
    int id;//产品编号

    public Chicken(int id) {
        this.id = id;
    }
}

//4.缓冲区
class SynContainer {
    //缓冲区大小
    Chicken[] chickens=new Chicken[10];
    //容器所存产品数
    int count=0;

    //======生产者生产产品方法======
    public synchronized void push(Chicken chicken){
        //满了就等待消费，不生产
        if(count==chickens.length){
            //通知消费者消费，生产者等待
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        //没满就生产
        chickens[count]=chicken;
        count++;
        //通知消费者消费
        this.notifyAll();

    }

    //======消费者消费产品方法======
    public synchronized Chicken pop(){
        //判断能否消费
        if(count==0){
            //不能消费，等待生产
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        //可以就消费
        count--;
        Chicken chicken = chickens[count];
        //吃完了，通知生产者生产
        this.notifyAll();

        return chicken;
    }

}