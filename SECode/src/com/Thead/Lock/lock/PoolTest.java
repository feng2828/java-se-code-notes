package com.Thead.Lock.lock;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PoolTest {
    public static void main(String[] args) {
        //1.创建线程池,大小为10
        ExecutorService pool= Executors.newFixedThreadPool(10);
        //2.线程丢入池子，参数是实现Runnable的实现类
        pool.execute(new MyThread());
        pool.execute(new MyThread());
        pool.execute(new MyThread());
        pool.execute(new MyThread());
        //3.关闭池子
        pool.shutdown();
    }
}
class MyThread implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println(Thread.currentThread().getName()+i);
        }
    }
}