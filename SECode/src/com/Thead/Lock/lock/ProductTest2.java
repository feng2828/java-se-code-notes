package com.Thead.Lock.lock;

//生产者消费者模型2====>信号灯法
public class ProductTest2 {
    public static void main(String[] args) {
        TV tv = new TV();
        new Actor(tv).start();
        new Watcher(tv).start();
    }
}
//演员
class Actor extends Thread{
    TV tv;
    public Actor (TV tv){
        this.tv=tv;
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            if (i%2==0){
                tv.play("你好！");
            }else {
                tv.play("不好！");
            }
        }
    }
}

//观众
class Watcher extends Thread{
    TV tv;
    public Watcher (TV tv){
        this.tv=tv;
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            tv.watch();
        }
    }
}

//节目
class TV {
    //节目名子
    String movie;
    //标志位:默认是真
    //演员表演，观众等待  T
    //观众观看，演员等待  F
    boolean flag=true;

    //表演
    public synchronized void play(String movie){
        if(!flag){
            //观众观看，演员等待
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("演员表演了"+movie);
        this.movie=movie;
        //通知观众观看
        this.notifyAll();//通知唤醒
        this.flag=!this.flag;

    }
    //观看
    public synchronized void watch(){
        if(flag){
            //演员表演，观众等待
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("观众观看了"+movie);
        //通知演员表演
        this.notifyAll();
        this.flag=!this.flag;
    }



}
