package com.Thead.Lock.lock;

import java.util.concurrent.locks.ReentrantLock;

//测试Lock锁
public class LockTest {
    public static void main(String[] args) {
        LockTest2 lockTest2 = new LockTest2();
        new Thread(lockTest2,"1").start();
        new Thread(lockTest2,"2").start();
        new Thread(lockTest2,"3").start();
    }
}
class LockTest2 implements Runnable{
    int ticketNums=1000;
    //定义Lock锁
    private final ReentrantLock lock=new ReentrantLock();
     @Override
    public void run() {
        while (true){
            try {
                lock.lock();//加锁
                if(ticketNums>0){
                    System.out.println(Thread.currentThread().getName()+"票有"+ticketNums--);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }else {
                    break;
                }
            } finally {
               //解锁
                lock.unlock();
            }
        }
    }
}