package com.Thead.Demo1;

//总结:注意,线程开启不一定立即执行,由CPU调度执行 I
public class ThreadDemo01 extends Thread{
    @Override
    public void run() {
        for (int i = 0; i <100 ; i++) {
            System.out.println("牛蛙"+i);
        }
    }

    public static void main(String[] args) {
        ThreadDemo01 threadDemo01 = new ThreadDemo01();
        threadDemo01.start();
        for (int i = 0; i < 200; i++) {
            System.out.println("厉害"+i);
        }
    }
}
