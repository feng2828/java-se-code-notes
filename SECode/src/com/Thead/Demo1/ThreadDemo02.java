package com.Thead.Demo1;

//创建线程方式2
public class ThreadDemo02 implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i <100 ; i++) {
            System.out.println("牛蛙"+i);
        }
    }

    public static void main(String[] args) {
        ThreadDemo02 threadDemo02 = new ThreadDemo02();
        //创建线程对象。开启线程
        Thread thread = new Thread(threadDemo02);
        thread.start();
        for (int i = 0; i < 200; i++) {
            System.out.println("厉害"+i);
        }
    }
}
