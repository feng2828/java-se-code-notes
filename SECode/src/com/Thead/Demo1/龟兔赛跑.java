package com.Thead.Demo1;

public class 龟兔赛跑 implements Runnable{

    private static String winner;//胜利者


    @Override
    public void run() {
        //定义一个赛道，每一次走一米
        for (int i = 0; i <=100; i++) {

            //模拟兔子休息,每十步判断一次
            if(Thread.currentThread().getName().equals("兔子")&&i%10==0){
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            boolean gameover=Gameover(i);
            if(gameover){
                break;
            }
            System.out.println(Thread.currentThread().getName()+"跑了"+i+"步");
        }
    }
    public Boolean Gameover(int steps){
        if (winner!=null){
            return true;//胜利者不为空
        }
        if(steps>=100){
            winner=Thread.currentThread().getName();//给先到终点的选手赋值
            System.out.println(winner+"赢了");
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        龟兔赛跑 thead1 = new 龟兔赛跑();
        new Thread(thead1,"兔子").start();
        new Thread(thead1,"乌龟").start();
    }

}
