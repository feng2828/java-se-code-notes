package com.Thead.Demo1;

//卖火车票
public class ThreadDemo03 implements Runnable {
    private int ticket =10;
    @Override
    public void run() {
        while (true){
            if(ticket<=0){
                break;
            }
            System.out.println(Thread.currentThread().getName()+"买了第"+ticket--+"张票");
        }
    }

    public static void main(String[] args) {
        ThreadDemo03 threadDemo03 = new ThreadDemo03();
        new Thread(threadDemo03,"峰").start();
        new Thread(threadDemo03,"宝").start();
        new Thread(threadDemo03,"憨憨").start();

    }
}
