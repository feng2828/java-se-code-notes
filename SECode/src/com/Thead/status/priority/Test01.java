package com.Thead.status.priority;

//测试线程的优先级
public class Test01 {
    public static void main(String[] args) {
        //主线程默认优先级为最大 5
        System.out.println(Thread.currentThread().getName()+"===>"+Thread.currentThread().getPriority());
        MyPriority myPriority = new MyPriority();
        Thread thread1 = new Thread(myPriority,"hhh1");
        Thread thread2 = new Thread(myPriority,"hhh2");
        Thread thread3 = new Thread(myPriority,"hhh3");
        Thread thread4 = new Thread(myPriority,"hhh4");
        Thread thread5 = new Thread(myPriority,"hhh5");
        //要先设置优先级再启动线程
        thread1.start();

        thread2.setPriority(1);
        thread2.start();

        thread3.setPriority(Thread.MAX_PRIORITY);//最大优先级10
        thread3.start();

        thread4.setPriority(4);
        thread4.start();

        thread5.setPriority(6);
        thread5.start();


    }
}
class MyPriority implements Runnable{

    @Override
    public void run() {
        //输出当前线程的名字和优先级
        System.out.println(Thread.currentThread().getName()+"===>"+Thread.currentThread().getPriority());
    }
}
