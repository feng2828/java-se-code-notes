package com.Thead.status;
//观察线程的状态
public class StateTest {
    public static void main(String[] args) throws InterruptedException {
        Thread thread=new Thread(()->{
            for (int i = 0; i < 2;i++) {
                try {
                    Thread.sleep(1000);//TIMED_WAITING
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("///////");
            }
        });

        //观察启动前状态
        Thread.State state=thread.getState();
        System.out.println(state);//new
        ////观察启动后状态
        thread.start();
        state=thread.getState();
        System.out.println(state);//Run
        //只要线程不终止，就一直输出状态
        while (state!=Thread.State.TERMINATED){
            Thread.sleep(100);
            state=thread.getState();//更新线程状态
            System.out.println(state);

        }
    }
}
