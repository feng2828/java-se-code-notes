package com.Thead.status;
//测试礼让线程
//礼让不一定成功
//这里注意无法保证谁先执行，
public class YieldDemo {
    public static void main(String[] args) {
        MyYield myYield = new MyYield();
        new Thread(myYield,"一号线程").start();
        new Thread(myYield,"二号线程").start();
    }
}
class MyYield implements Runnable{
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"线程开始！");
        Thread.yield();//线程礼让
        System.out.println(Thread.currentThread().getName()+"线程结束！");
    }
}