package com.Thead.status;

import com.Thead.Demo1.ThreadDemo03;

//模拟网络并发：放大问题的发生性
public class SleepDemo implements Runnable{
    private int ticket =10;
    @Override
    public void run() {
        while (true){
            if(ticket<=0){
                break;
            }
            //模拟延时
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName()+"买了第"+ticket--+"张票");
        }
    }

    public static void main(String[] args) {
        ThreadDemo03 threadDemo03 = new ThreadDemo03();
        new Thread(threadDemo03,"峰").start();
        new Thread(threadDemo03,"宝").start();
        new Thread(threadDemo03,"憨憨").start();

    }
}
