package com.Thead.status;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.SimpleFormatter;

//模拟倒计时
public class StopTest2 {

    public static void main(String[] args) {
        StopTest2 stopTest2 = new StopTest2();
        stopTest2.tenDown();

        //获取系统当前时间
        Date date = new Date(System.currentTimeMillis());
        String s = new SimpleDateFormat("HH:mm:ss").format(date);//转换格式
        System.out.println(s);
    }
    public void tenDown(){
        int nums=10;
        while (true){
            try {
                Thread.sleep(1000);
                System.out.println(nums--);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (nums<=0){
                break;
            }
        }
    }
}
