package com.Thead.status;
//守护线程测试类
//上帝守护你
public class DeamonTest {
    public static void main(String[] args) {
        God god = new God();
        You you = new You();

        Thread threadGod = new Thread(god);
        threadGod.setDaemon(true); //将上帝变成守护线程.
        threadGod.start();

        Thread threadYou = new Thread(you);
        threadYou.setDaemon(false);//默认是false，表示用户线程，也就是普通线程
        threadYou.start();
    }
}

//上帝
class God implements Runnable{
    @Override
    public void run() {
        while (true){
            System.out.println("无敌");
        }
    }
}

//你
class You implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("开心每一天！");
        }
        System.out.println("奈斯");
    }
}
