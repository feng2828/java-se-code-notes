package com.Thead.status;

public class JoinTest implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            System.out.println("线程VIP"+i);
        }
    }

    public static void main(String[] args) {
        JoinTest joinTest = new JoinTest();
        Thread thread = new Thread(joinTest);
        thread.start();
        //主线程方法
        for (int i = 0; i < 500; i++) {
            if (i==200){
                try {
                    thread.join();//让run方法的线程插队
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("main"+i);
        }
    }
}
