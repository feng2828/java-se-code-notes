package com.feng.两数之和;

import java.util.HashMap;

/*
*
* 给定一个整数数组 nums[]和一个整数目标值 target，
*
* 请你在该数组中找出 和为目标值 target的那两个整数，并返回它们的数组下标。

你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。

你可以按任意顺序返回答案。

*
*
* */
public class Solution {
    //暴力法就是 遍历num[i],再遍历num[i+1]=target-num[i],时间复杂度为n2 平方
    //map使用两遍的方法，map先put一遍，再查询匹配一遍，这个复杂度为 n
    public static int [] getIndex(int[] nums,int target ){

        int[] indexs = new int[2];//定义返回的索引下标组
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < nums.length; i++) {
            map.put(nums[i],i);
        }
        for (int i = 0; i < nums.length; i++) {
            int complement =target-nums[i];//定义一个剩余的值
            //用Map来查询匹配并且防止Map[i]为值
            if(map.containsKey(complement)&&map.get(complement)!=i){
                indexs[0]=i;
                indexs[1]=map.get(complement);
            }
        }
        return indexs;
    }


    //map一遍的方法,相当于一边put进去，一边查询
    public static int[] twoSum(int[] nums, int target) {
        int[] indexs = new int[2];
        HashMap<Integer,Integer> hash = new HashMap<Integer,Integer>();
        for(int i = 0; i < nums.length; i++){
            if(hash.containsKey(nums[i])){
                indexs[0] = i;
                indexs[1] = hash.get(nums[i]);
                return indexs;
            }
            hash.put(target-nums[i],i);
        }
        return indexs;
    }

    public static void main(String[] args) {
        int[] i={2,7,11,15};
        int[] index = twoSum(i, 9);
        System.out.println(index[0]);
        System.out.println(index[1]);
    }
}
